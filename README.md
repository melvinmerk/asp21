# ASP Labo carte SD
### Auteur: Melvin Merk & Gabrielle Thurnherr
### Date: 03/06/2021

# Solutions de départ
Pour le départ de ce labo nous avons utilisé les fichiers objets fourni. Cependant en cours de route, notamment pour la partie Moniteur, nous avons utilisé notre propre librairie LCD car il fallait implémenter le retour à la ligne.

# Fonctions bas niveau
## Etape 1: Initialisation carte SD
---
Solution fournie.

## Etape 2: Lire et afficher informations carte SD
---
Les informations de la carte SD sont stockées dans les registres cid et csd.  
Il a fallu extraire les informations voulues de ces deux registres en s'aidant des specifications de la carte SD.

## Etape 3: Lecture d'un block
---
Le flow d'une lecture de block est décrite dans le manuel du DM3730 chapitre 24.5.2.4.  
Nous avons juste suivi ces indications pour effectuer une lecture.  
Les bloc 1 contient un comptage de 0x00 à 0xFF (deux fois).

Le bloc 2 contient `0x0000200: 4567 0123 cdef 89ab 4567 0123 cdef 89ab` en "boucle".

Le bloc 3 contient `0x0000400: babe cafe babe cafe babe cafe babe cafe` en "boucle".

Le bloc 4 contient `0x0000600: beaf dead beaf dead beaf dead beaf dead` en "boucle".


## Etape 4: Lecture de plusieurs blocks
---
La lecture de plusieurs blocks est implémentée de la même façon que le single block, la seule différence est la commande envoyée ainsi que l'envoie de la CMD 12 après lecture des blocks pour indiquer à la carte SD que le transfert est terminé.

## Etape 5: Écriture d'un block
---
Le flow d'écriture est décris au même endroit que pour la lecture la seule différence est le bit à poller pour le buffer.

## Etape 6: Écriture de plusieurs blocks
---
Pareil que pour la lecture il faut envoyer la CMD 12 en fin de transfert.  

# Moniteur

## Etape 1: Lister les fichiers
---
Pour lister les fichiers nous nous sommes inspirés de l'exemple donné dans la documentation FatFs.  (*scan_files*).

## Etape 2: Lire un fichier
---
Nous avons implémenter le retour à la ligne de l'écran dans cette partie. Pour un retour à la ligne explicite (ascii 13) ou pour un débordement de l'écran.

## Etape 3: Ecrire un fichier
---
Dans cette partie nous avons implémenter la fonction *create_file(...)* qui permet de créer un fichier. Cette fonction est très basique car elle crée simplement le fichier avec le contenu fourni. Si le fichier existe déjà ce dernier sera simplement écrasé avec le nouveau contenu. 

## Etape 4: Informations fichier
---
Dans cette partie nous avons implémenter la fonction *print_file_info(...)* qui affiche les informations d'un fichier. Nous avons choisi d'afficher uniquement le nom, la taille et la date de création.

---
# ASP Labo peripherals
### Auteur: Melvin Merk
### Date: 25/04/2021

## Etape 1: LCD
---
Cette étape consistait à mettre en place une librairie de fonctions qui interagit avec l'écran LCD.  
Il s'agissait d'écrire correctement un pixel dans le framebuffer en utilisant la macro LCD_BUF(x).  
Puis tout les autres fonctions dérivent de la fonction set pixel.

## Etape 2: GPIOs
---
Dans cette étape il fallait mettre en place une librairie pour gérer des entrées et sorties "classique" (GPIOs).  
Pour cela il fallait premièrement configurer les padconfs, pour utiliser une certaines pin comme une GPIO.  
Puis configurer le module GPIO lui même, à savoir si la pin est une sortie ou une entrée. 
Finalement créer les fonctions d'accès au gpios (SetOutput, ReadInput, etc...)

## Etape 3: Interruptions
---
Dans cette étape il fallait configurer la partie interruptions des GPIO.  
Pour cela il fallait: 
- créer une routine d'interruption en assembleur (qui appelle une routine en C)
- activer les IRQ des gpios (registres IRQENABLE). sur le flanc montant (registre RISINGDETECT)
- démasquer les interruptions pour notre module GPIO (registre INTC_MIR_CLEAR1)
- accepter les interruptions par le contôleur d'interruption (registre INTC_CONTROL)
- activer les interruptions par le CPSR

## Etape 4: Timer
---
Dans cette étape il fallait mettre en place une librairie de gestion d'un timer.  
Premièrement, il fallait initialiser le timer:  
- Sélectionner le clock (ici 32kHz)
- Activer le "functional clock"
- activer le "interface clock"

Puis créer les fonctions de la librairie:  
- Démarrer/stopper le timer
- Lire la valeur
- Écrire la valeur

## Etape 5: Application
---
Dans cette étape finale il s'agissait de mettre en place un petit jeu de temps de réaction.  
Le principe est que une led s'allument après un temps aléatoire, et que deux joueurs s'affrontent,  
chacun ayant son propre bouton, le joueur appuyant le plus vite sur le bouton dès que la leds s'est allumée gagne la partie.  

La valeur aléatoire n'est que pseudo aléatoire car le timer est utilisé pour obtenir le temps avant que la leds s'allume.  
Une partie se lance en pressant sur le bouton SW2, puis les boutons SW0 et SW1 sont respectivement les boutons des joueurs 1 et 2.

Lorsqu'un joueur presse avant que la led ne se soit allumé un message apparaît pour indiquer que le joueur a pressé trop tôt.  
Sinon le vainqueur est annoncé et son temps de réaction est affiché en millisecondes.

