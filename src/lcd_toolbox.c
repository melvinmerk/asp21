/* ----------------------------------------------------------------------
   Institut ReDS - HEIG-VD Yverdon-les-Bains
   ------------------------------------------------------------------------

   Auteur : Evangelina LOLIVIER-EXLER
   Email  : evangelina.lolivier-exler@heig-vd.ch
   Date   : 17/09/2008
   Modified: 24/05/2012 REPTAR board adaptation
   Description : LCD toolbox
   -----------------------------------------------------------------------
*/

#include "stddefs.h"
#include "lcd.h"
#include "vga_lookup.h"
#include "fb_fonts.h"
#include "gpio.h"
#include "prcm.h"
#include "bits.h"
#include "lcd_toolbox.h"

#define LINES_PER_SCREEN        (PIXELS_PER_COL/FONT_HEIGHT)
#define GET_IDX_FROM_XY(x,y)    (x + y * PIXELS_PER_ROW)

#define VALID_XY(x, y)          (x < PIXELS_PER_ROW || y < PIXELS_PER_COL)

// globals to keep track of foreground, background colors and x,y position
uchar lcd_color_depth=16;       // 16 bits per pixel
uchar lcd_bg_color=3;           // 0 to 15, used as lookup into VGA color table
uchar lcd_fg_color=8;           // 0 to 15, used as lookup into VGA color table

ulong line_nb = 0;
ulong current_y = 0;

//------------------------------------------------------------------------------
// Delay for some usecs. - Not accurate
// and no Cache
void udelay(int delay)
{
    volatile int i;
    for ( i = LOOPS_PER_USEC * delay; i ; i--)
        ;
}



///--------------------------------------------------------------------------
// lcd_on
//
// This function turns on the DM3730 Display Controller
//
void lcd_on()
{

    // Enable the clocks to the DISPC
    // enable functional clock (will be enable with the LCD_on function)
    DSS_CM_REG(CM_FCLKEN)|=BIT0;
    // enable interfaces clocks (L3 and L4)  (will be enable with the LCD_on function)
    DSS_CM_REG(CM_ICLKEN)|=BIT0;

    // power up the LCD
    udelay(10000);
    // LCD output enable
    LCD_REG(control)|= BIT0;
    udelay(10000);

}

//--------------------------------------------------------------------------
// lcd_off
//
// This function turns off the DM3730 Display Controller
//
void lcd_off()
{
    // power down the LCD
    udelay(10000);

    // digital output disable
    LCD_REG(control)&=~BIT1;
    // LCD output disable
    LCD_REG(control)&=~BIT0;
    udelay(10000);
}




// ------------------------------------------------------------------
// get_pixel_add return the address of a pixel with respect to the frame buffer start address
ulong get_pixel_add (ulong X, ulong Y)
{
    if(!VALID_XY(X, Y))
        return 0;
    return GET_IDX_FROM_XY(X, Y) * 2;
}

// ------------------------------------------------------------------
// fb_set_pixel sets a pixel to the specified color (between 0 et 15).
void fb_set_pixel(ulong X, ulong Y, uchar color)
{
    if(color > 15 || !VALID_XY(X, Y)) // Cant be lower than 0 since unsigned
        return;
    LCD_BUF(get_pixel_add(X, Y)) = vga_lookup[color];
}



// ------------------------------------------------------------------
// fb_print_char prints a character at the specified location.
//
void fb_print_char(uchar cchar, ulong x, ulong y, uchar color)
{
    ulong i, j;

    for(i = 0; i < FONT_HEIGHT; ++i) {
        for(j = 0; j < FONT_WIDTH; ++j) {
            if((fb_font_data[cchar-FIRST_CHAR][i] << j) & 0x80)
                fb_set_pixel(x + j, y + i, color);
        }
    }
}

// ------------------------------------------------------------------
// fb_print_string prints a string at the specified location.(30 characters max)
//
void fb_print_string(uchar *pcbuffer, ulong x, ulong y, uchar color)
{
    ulong i = 0;
    while(*(pcbuffer+i) != 0) {
        if(*(pcbuffer+i) == 13 || x == PIXELS_PER_ROW) {// retour ligne
            line_nb++;
            x = 0;
            y = get_current_y(line_nb);
            i++;

            if(*(pcbuffer+i) == 10) // line feed
                i++;
        }

        fb_print_char(*(pcbuffer+i), x, y, color);
        x += FONT_WIDTH;
        i++;
        if(i > 2000)
            break;
    }
}
// ------------------------------------------------------------------
//
// This function clear the screen
//
void clear_screen()
{
    ulong i, j;
    for(i = 0; i < PIXELS_PER_ROW; ++i) {
        for(j = 0; j < PIXELS_PER_COL; ++j) {
            fb_set_pixel(i, j, lcd_bg_color);
        }
    }

    line_nb = 0;
}

ulong get_current_y(ulong line_nb) {
    return line_nb * 16;
}

