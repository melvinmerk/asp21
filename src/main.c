/*---------------------------------------------------------------------------------------
 * But               : Petit programme pour etre charge dans le micro et affiche avec dump
 * Auteur            : Evangelina LOLIVIER-EXLER
 * Date              : 29.07.2008
 * Version           : 1.0
 * Fichier           : demo.c
 *----------------------------------------------------------------------------------------*/
#include "cfg.h"
#include "stddefs.h"

#include "init.h"
#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "bits.h"
#include "stdio.h"
#include "timer_toolbox.h"
#include "gpio.h"
#include "intc.h"
#include "application.h"
#include "sd_toolbox.h"
#include "mmchs.h"
#include "ff.h"
#include "menu_toolbox.h"
#include "test_menu.h"

/* Global variables */
vulong AppStack_svr[APPSTACKSIZE/4];
vulong AppStack_irq[APPSTACKSIZE/4];


void isr_handler(void) {

    // Reset IRQ status for sw 1 and 2
    GPIO5_REG(OMAP_GPIO_IRQSTATUS1) |= SW0_BITMASK | SW1_BITMASK;
    // accept interrupt
    MPU_INTC_REG(INTC_CONTROL) |= BIT0;
}



static void general_init(void) {
    lcd_off();
    lcd_init();
    lcd_on();
    GPIO_init();
    timer_init();
    mmc1_init();
    //interrupt_init();
}



/* main */
int main(void)
{
    general_init();
    //clear LCD
    clear_screen();



    test_menu();


    while(1) {


    }

    return(0);
}
