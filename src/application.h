/*
 * application.h
 *
 *  Created on: Apr 23, 2021
 *      Author: Melvin Merk
 */

#ifndef SRC_APPLICATION_H_
#define SRC_APPLICATION_H_

#define LED0_MODULE     5
#define LED1_MODULE     5
#define LED0_BITMASK    BIT15
#define LED1_BITMASK    BIT13

#define SW0_MODULE     5
#define SW1_MODULE     5
#define SW2_MODULE     6
#define SW0_BITMASK    BIT12
#define SW1_BITMASK    BIT14
#define SW2_BITMASK    BIT7

extern void app_main_init(void);
extern void app_main(void);
extern void app_isr_cb(void);
extern void delay_ms(ulong nbIter);

#endif /* SRC_APPLICATION_H_ */
