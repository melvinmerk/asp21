/*
 * timer_toolbox.h
 *
 *  Created on: Apr 23, 2021
 *      Author: Melvin Merk
 */

#ifndef SRC_TIMER_TOOLBOX_H_
#define SRC_TIMER_TOOLBOX_H_

#include "stddefs.h"

extern void start_timer(void);
extern void stop_timer(void);
extern ulong read_timer_value(void);
extern void write_timer_value(ulong val);


#endif /* SRC_TIMER_TOOLBOX_H_ */
