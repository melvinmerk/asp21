/*
 * application.c
 *
 *  Created on: Apr 23, 2021
 *      Author: Melvin Merk
 */
#include <string.h>
#include <stdio.h>

#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "bits.h"

#include "timer_toolbox.h"
#include "gpio.h"
#include "application.h"


// Approx 9s.
#define MAX_WAIT    (9000)
#define MIN_WAIT    (1000)

#define TICKS_P_MSEC   (32.768)
#define TICKS_TO_MSEC(ticks)   ((ticks/TICKS_P_MSEC))

static volatile uchar gameStarted = 0;
static volatile uchar ledOn = 0;

/**
 * Function used to get a very pseudo random number using the timer value
 */
static ulong pseudo_rand(void) {
    return MIN_WAIT + read_timer_value() % MAX_WAIT;
}

void app_main_init(void) {
    // Start timer
    write_timer_value(0);
    start_timer();
    gameStarted = 0;
    ledOn = 0;
    fb_print_string((uchar *)"Start a game with SW2!", 2, 2, 0);
}

void app_main() {

    if(!gameStarted) {

        if(ReadInput(SW2_MODULE, SW2_BITMASK)) { // Start game
            clear_screen();
            fb_print_string((uchar *)"Be ready!", 2, 2, 0);
            gameStarted = 1;
            // wait "random" time
            delay_ms(pseudo_rand());
            // Turn led on
            SetOutput(LED0_MODULE, LED0_BITMASK);
            ledOn = 1;
            // Reset timer to count reaction time
            write_timer_value(0);
        }
    }

    // Reset timer value (since timer is not configured as auto-reload)
    if(read_timer_value() == 0xFFFFFFFF) {
        write_timer_value(0);
    }
}

static void printWinner(uchar *winner, ulong reactionTimeMs) {
    uchar buffer[300];


    sprintf((char *)buffer, "The winner is %s.", winner);
    fb_print_string(buffer, 2, 15, 0);
    sprintf((char *)buffer, "Reaction time: %lu [ms]!", reactionTimeMs);

    fb_print_string(buffer, 2, 30, 0);
}

static void printCheater(uchar *loser) {
    uchar buffer[300];

    sprintf((char *)buffer, "Too soon %s.", loser);
    fb_print_string(buffer, 2, 15, 0);
}


void app_isr_cb(void) {
    uchar player[] = "Player 1";

    if(!gameStarted) {
        return;
    }

    if(GPIO5_REG(OMAP_GPIO_IRQSTATUS1) & SW0_BITMASK) { // player 1
        // Nothing to do
    } else if(GPIO5_REG(OMAP_GPIO_IRQSTATUS1) & SW1_BITMASK) { // player 2
        player[7] = '2';
    }

    // If led isnt on the player pressed too early
    if(!ledOn) {
        printCheater(player);
        return;
    }

    clear_screen();
    fb_print_string((uchar *)"Start a new game with SW2!", 2, 2, 0);

    ulong reactionTimeTicks = read_timer_value();
    ulong reactionTimeMs = TICKS_TO_MSEC(reactionTimeTicks);

    printWinner(player, reactionTimeMs);

    gameStarted = 0;
    ledOn = 0;
    ClearOutput(LED0_MODULE, LED0_BITMASK);

}


// Not exactly ms, a little more but its not important
void delay_ms(ulong nbIt) {
    ulong i, j;
    for(i = 0; i < nbIt; i++)
        for(j = 0; j < 100000; j++){}
}

