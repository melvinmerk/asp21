/*
 * fat_toolbox.c
 *
 *  Created on: 12 nov. 2012
 *      Author: Evangelina Lolivier-Exler
 */

#include "stddefs.h"
#include "ff.h"
#include "lcd_toolbox.h"
#include "lcd.h"
#include "fat_toolbox.h"
#include <string.h>
#include <stdio.h>

/* data buffer, size : 10 clusters, 1 cluster = 8 blocks of 512 B */
uchar buff[MAX_FILE_SIZE];

int create_file(char *file_name, uchar *data, ulong nbyte)
{
    FIL fp;

    ushort i;
    WORD bw = 0;
    WORD nbBytes = 0;
    WORD nbBlocks;

    uchar block[512];

    // ouverture du fichier
    FRESULT fr = f_open(&fp, (const char *)file_name, FA_CREATE_ALWAYS | FA_WRITE);

    if(fr != FR_OK)
        return fr;

    // Calcul du nombre de bloc à écrire
    nbBlocks = (nbyte % 512) != 0 ? (nbyte / 512) + 1 : (nbyte / 512);

    if(nbBlocks > 8) { // Too many blocks
        f_close(&fp);
        return FR_DENIED;
    }

    for(i = 0; i < nbBlocks; i++) {
        if(i == nbBlocks-1) {
            nbBytes = nbyte % 512;
        } else {
            nbBytes = 512;
        }
        memcpy(block, &data[i * 512],  nbBytes);

        // écriture des données
        fr = f_write(&fp, block, nbBytes, &bw);

        if(fr != FR_OK) {
            f_close(&fp);
            return fr;
        }
    }
    f_close(&fp);

    return 0;
}

/* path is "" for the root directory */
void scan_files (char *path)
{
    FILINFO finfo;
    DIR dirs;
    uchar buff[100];
    clear_screen();

    if (f_opendir(&dirs, path) == FR_OK) {

        while ((f_readdir(&dirs, &finfo) == FR_OK) && finfo.fname[0]) {
            if (finfo.fattrib & AM_DIR) {
                 // on ignore les directories   
            } else {
                sprintf((char *)buff, "%s/%s\r", path, &finfo.fname[0]);
                fb_print_string(buff, 0, get_current_y(line_nb), lcd_fg_color);
                line_nb++;
            }
        }
    }
}

int read_file(char *file_name)
{
    clear_screen();

    FIL fp;
    WORD nbBytes = 0;
    ushort i;
    WORD br = 0;
    uchar block[512];
    
    // Ouverture du fichier
    FRESULT fr = f_open(&fp, (const char *)file_name, FA_READ);

    if(fr != FR_OK)
        return fr;

    for(i = 0; i < 8; i++) {
        // Lecture des données
        fr = f_read(&fp, block, 512, &br);

        memcpy(&buff[i * 512], block, br);

        if(fr != FR_OK) {
            f_close(&fp);
            return fr;
        }
        nbBytes += br;

        if(br < 512)
            break;
    }
    f_close(&fp);


    // Evite de lire plus de byte dans buff
    buff[nbBytes] = 0;

    // affichage du contenu
    fb_print_string(buff,0, get_current_y(line_nb), lcd_fg_color);

    return fr;
}

int print_file_info(char *file_name)
{
    clear_screen();

    FILINFO finfo;
    uchar lcd_line[100];
    FRESULT fr = f_stat(file_name, &finfo);

    if(fr != FR_OK)
        return fr;

    // Affichage des infos (seulement nom, taille et date, dans notre cas)
    sprintf((char *)lcd_line, "File name: %s\rSize: %d\rDate: %d",&finfo.fname[0], finfo.fsize, finfo.fdate);
    fb_print_string(lcd_line,0, get_current_y(line_nb), lcd_fg_color);

    return 0;
}
