/*
 * timer_toolbox.c
 *
 *  Created on: Apr 23, 2021
 *      Author: Melvin Merk
 */

#include "bits.h"
#include "stddefs.h"
#include "timer.h"

void start_timer(void) {
    GPT1_REG(TCLR) |= BIT0;
}

void stop_timer(void) {
    GPT1_REG(TCLR) &= ~BIT0;
}

ulong read_timer_value(void) {
    return GPT1_REG(TCRR);
}

void write_timer_value(ulong val) {
    GPT1_REG(TCRR) = val;
}
