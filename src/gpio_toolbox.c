/*
 * gpio_toolbox.c
 *
 *  Created on: Mar 9, 2021
 *      Author: Melvin Merk
 */
#include "stddefs.h"
#include "gpio.h"
#include "bits.h"

void SetOutput(uchar module, ulong bitmask) {
    switch(module) {
        case 1:
            GPIO1_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
            break;
        case 2:
            GPIO2_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
            break;
        case 3:
            GPIO3_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
            break;
        case 4:
            GPIO4_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
            break;
        case 5:
            GPIO5_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
            break;
        case 6:
            GPIO6_REG(OMAP_GPIO_SETDATAOUT) = bitmask;
            break;
        default:
            break;
    }
}

void ClearOutput(uchar module, ulong bitmask) {
    switch(module) {
        case 1:
            GPIO1_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
            break;
        case 2:
            GPIO2_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
            break;
        case 3:
            GPIO3_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
            break;
        case 4:
            GPIO4_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
            break;
        case 5:
            GPIO5_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
            break;
        case 6:
            GPIO6_REG(OMAP_GPIO_CLEARDATAOUT) = bitmask;
            break;
        default:
            break;
    }
}
uchar ReadInput(uchar module, ulong bitmask) {
    switch(module) {
        case 1:
            return (GPIO1_REG(OMAP_GPIO_DATAIN) & bitmask) == bitmask;
        case 2:
            return (GPIO2_REG(OMAP_GPIO_DATAIN) & bitmask) == bitmask;
        case 3:
            return (GPIO3_REG(OMAP_GPIO_DATAIN) & bitmask) == bitmask;
        case 4:
            return (GPIO4_REG(OMAP_GPIO_DATAIN) & bitmask) == bitmask;
        case 5:
            return (GPIO5_REG(OMAP_GPIO_DATAIN) & bitmask) == bitmask;
        case 6:
            return (GPIO6_REG(OMAP_GPIO_DATAIN) & bitmask) == bitmask;
        default:
            return 0;
    }
}

void ToggleOutput(uchar module, ulong bitmask) {
    switch(module) {
        case 1:
            if(GPIO1_REG(OMAP_GPIO_DATAOUT) & bitmask)
                ClearOutput(module, bitmask);
            else
                SetOutput(module, bitmask);
            break;
        case 2:
            if(GPIO2_REG(OMAP_GPIO_DATAOUT) & bitmask)
                ClearOutput(module, bitmask);
            else
                SetOutput(module, bitmask);
            break;
        case 3:
            if(GPIO3_REG(OMAP_GPIO_DATAOUT) & bitmask)
                ClearOutput(module, bitmask);
            else
                SetOutput(module, bitmask);
            break;
        case 4:
            if(GPIO4_REG(OMAP_GPIO_DATAOUT) & bitmask)
                ClearOutput(module, bitmask);
            else
                SetOutput(module, bitmask);
            break;
        case 5:
            if(GPIO5_REG(OMAP_GPIO_DATAOUT) & bitmask)
                ClearOutput(module, bitmask);
            else
                SetOutput(module, bitmask);
            break;
        case 6:
            if(GPIO6_REG(OMAP_GPIO_DATAOUT) & bitmask)
                ClearOutput(module, bitmask);
            else
                SetOutput(module, bitmask);
            break;
        default:
            break;
       }
}

